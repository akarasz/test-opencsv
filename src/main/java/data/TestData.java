package data;

/**
 * @author karaszandris
 */
public class TestData {

    private String param1;
    private int param2;
    private String param3;
    private String param4;

    private String extraData1;
    private int extraData2;

//    public String getParam1() {
//        return param1;
//    }
//
//    public void setParam1(String param1) {
//        this.param1 = param1;
//    }
//
//    public int getParam2() {
//        return param2;
//    }
//
//    public void setParam2(int param2) {
//        this.param2 = param2;
//    }
//
//    public String getParam3() {
//        return param3;
//    }
//
//    public void setParam3(String param3) {
//        this.param3 = param3;
//    }
//
//    public String getParam4() {
//        return param4;
//    }
//
//    public void setParam4(String param4) {
//        this.param4 = param4;
//    }

    public String getExtraData1() {
        return extraData1;
    }

    public void setExtraData1(String extraData1) {
        this.extraData1 = extraData1;
    }

    public int getExtraData2() {
        return extraData2;
    }

    public void setExtraData2(int extraData2) {
        this.extraData2 = extraData2;
    }

    public String toString() {
        return String.format("TestData[param1=%s, param2=%s, param3=%s, param4=%s, extraData1=%s, extraData2=%s]", param1, param2, param3, param4, extraData1, extraData2);
    }

}
