import com.opencsv.CSVReader;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.opencsv.bean.IterableCSVToBean;
import data.TestData;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;

/**
 * @author karaszandris
 */
public class Application {

    public static void main(String[] args) {
        try (CSVReader reader = new CSVReader(new InputStreamReader(Application.class.getResourceAsStream("/majomfasz.csv")))) {
            HeaderColumnNameMappingStrategy strategy = new HeaderColumnNameMappingStrategy();
            strategy.setType(TestData.class);

            Iterator<TestData> iterator = new IterableCSVToBean(reader, strategy, null).iterator();

            // skip second line where the field types are defined
            try { iterator.next(); } catch (final Exception e) { }

            int i = 0;
            while (iterator.hasNext()) {
                TestData data = iterator.next();
                data.setExtraData1("baszod");
                data.setExtraData2(++i);

                System.out.println(data);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
